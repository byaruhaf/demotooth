//
//  ViewController.swift
//  DemoTooth
//
//  Created by Franklin Byaruhanga on 04/09/2020.
//

import UIKit
import HealthKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let store = HKHealthStore()
        // Create ToothbrushingEvent category type
        guard let categoryType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.toothbrushingEvent) else {
            fatalError("*** Unable to create a sleep analysis category type ***")
        }
        var shareTypes = Set<HKSampleType>()
        shareTypes.insert(categoryType)

        var readTypes = Set<HKObjectType>()
        readTypes.insert(categoryType)

        store.requestAuthorization(toShare: shareTypes, read: readTypes) { (success, error) -> Void in
            if success {
                print("success")
            } else {
                print("failure")
            }

            if let error = error { print(error) }
        }

        // Now create the sample
        let toothbrushingSample = HKCategorySample(
            type: categoryType,
            value: HKCategoryValue.notApplicable.rawValue,
            start: Date().addingTimeInterval(-10000),
            end: Date()
        )

        // Finally save to health store
        store.save(toothbrushingSample) { (result:Bool, error:Error?) in
            if result{
                print("Saved")
            }else{
                print("error saving toothBrushing",error?.localizedDescription ?? "")
            }
        }
    }

}

